# Git global setup

```shell
git config --global user.name "nicolas labbe"
git config --global user.email "nicolas.labbe@adfab.fr"
```

# Create a new repository

```shell
git clone https://gitlab.com/nlabbe/gitlab-workshop/
cd gitlab-workshop
touch README.md
git add README.md
git commit -m "add README"
git push -u origin master
```